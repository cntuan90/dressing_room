import data from "./data.js";

const getEle = (id) => document.getElementById(id);

const renderTopclothes = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "topclothes") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleTopClothes(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabTopClothes").innerHTML = arr;
  });
};
renderTopclothes();

const renderBotclothes = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "botclothes") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleBotClothes(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabBotClothes").innerHTML = arr;
  });
};
renderBotclothes();

const renderShoes = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "shoes") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleShoes(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabShoes").innerHTML = arr;
  });
};
renderShoes();

const renderHandBags = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "handbags") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleHandBags(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabHandBags").innerHTML = arr;
  });
};
renderHandBags();

const renderNecklaces = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "necklaces") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleNecklaces(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabNecklaces").innerHTML = arr;
  });
};
renderNecklaces();

const renderHairStyle = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "hairstyle") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleHairStyle(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabHairStyle").innerHTML = arr;
  });
};
renderHairStyle();

const renderBackground = () => {
  let arr = "";
  data.forEach((item, index) => {
    if (item.type === "background") {
      arr += `
        <div class="list">
            <img src=${item.imgSrc_jpg} />
            <p>${item.name}</p>
            <button onclick="handleBackground(${index})">Thử</button>
        </div>
        `;
    }
    getEle("tabBackground").innerHTML = arr;
  });
};
renderBackground();

const handleTopClothes = (id) =>
  (getEle("topclothes").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleTopClothes = handleTopClothes;

const handleBotClothes = (id) =>
  (getEle("botclothes").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleBotClothes = handleBotClothes;

const handleHairStyle = (id) =>
  (getEle("hairstyle").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleHairStyle = handleHairStyle;

const handleNecklaces = (id) =>
  (getEle("necklaces").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleNecklaces = handleNecklaces;

const handleHandBags = (id) =>
  (getEle("handbags").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleHandBags = handleHandBags;

const handleShoes = (id) =>
  (getEle("shoes").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleShoes = handleShoes;

const handleBackground = (id) =>
  (getEle("background").innerHTML = `<img src="${data[id].imgSrc_png}"/>`);
window.handleBackground = handleBackground;
